#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'hashdiff'
require 'pp'

module Gitlab
  # Module Utils
  module Utils
    # Class representing a project, contains all its settings
    class Project
      attr_reader :config

      def initialize(client, project_id)
        @client = client
        @config = {}
        @project_id = project_id
      end

      def update(global_config)
        add_config('general' => general)
        add_config('protected_branches' => protected_branches)
        add_config('git_hook' => git_hook)
        add_config('services' => { 'slack' => slack })
        add_config('runners' => runners)
        @profile = profile(global_config)
        self
      end

      def add_config(project_config)
        @config = Config.deep_merge(@config, project_config)
      end

      def match_config
        diff = HashDiff.diff(@config, @profile)
        diff = diff.map do |e|
          if e.first == '-' && (e[1] =~ /^[\w]*\[[\d]*\]$/) == 0
            ['+', "un#{e[1]}", e[2]]
          else
            e
          end
        end
        diff.reject { |e| e.first == '-' } # Remove what we did not specify
      end

      def print_diff
        name = @config['general']['name']
        puts "Checking configuration for #{name}…"
        diff = match_config
        to_print = diff.map { |line| format_diff_line(line) }
        if to_print.empty?
          puts "Nothing to do, #{name} is well configured"
        else
          puts "Configuration of #{name} is not up-to-date:"
          to_print.each { |line| puts "  #{line}" }
        end
      end

      def sync_config
        name = @config['general']['name']
        puts "Syncing configuration for #{name}…"
        diff = match_config
        actions = generate_actions(diff)
        unless actions.empty?
          puts "Configuration of #{name} is not up-to-date, applying:"
          pp actions
          execute(actions)
        end
      end

      def profile(global_config)
        group, project_name =
          @config['general']['path_with_namespace'].split('/')
        profile_name = 'default'
        global_config['groups'][group].each_pair do |name, projects|
          if projects == 'default' || projects.include?(project_name)
            profile_name = name
          end
        end
        profile = global_config['profiles'][profile_name]
        substitute(profile, group, project_name)
      end

      private

      def substitute(hash, group, project)
        to_merge = {}
        hash.each_pair do |key, value|
          to_merge[key] = substitute_value(value, group, project)
        end
        hash.merge(to_merge)
      end

      def substitute_value(value, group, project)
        case
        when value == '#group' then group
        when value == '#project' then project
        when value == '#allrunners' then allrunners
        when value.is_a?(Hash)
          substitute(value, group, project)
        else value
        end
      end

      def general
        @client.get("/projects/#{@project_id}")
      end

      def protected_branches
        branches = @client.get("/projects/#{@project_id}/repository/branches")
        branches = branches.map do |branch|
          branch['name'] if branch['protected']
        end
        branches.compact.sort
      end

      def git_hook
        @client.get("/projects/#{@project_id}/git_hook")
      end

      def slack
        @client.get("/projects/#{@project_id}/services/slack")
      end

      def runners
        runners = @client.get("/projects/#{@project_id}/runners")
        runners.reject! { |r| r['is_shared'] }
        runners = runners.map { |r| r['id'] }
        runners.sort
      end

      def allrunners
        runners = @client.get('/runners').reject { |r| r['is_shared'] }
        runners.map { |r| r['id'] }.sort
      end

      def format_diff_line(line)
        case line.first
        when '~'
          _, key, current, should = line
          "Key `#{key}' is `#{current}' but should be `#{should}'"
        when '+'
          _, key, value = line
          "Key `#{key}' is undefined but should be `#{value}'"
        else
          raise "Invalid configuration diff: `#{line}'"
        end
      end

      def generate_actions(diff)
        keys_map = {}
        diff.map do |line|
          key = line[1]
          value = line.last
          hash = create_hash(key.split('.'), value)
          keys_map = Config.deep_merge(keys_map, hash)
        end
        keys_map
      end

      def create_hash(keys, value)
        return value if keys == []
        { keys.first => create_hash(keys[1..-1], value) }
      end

      def execute(actions, sub = '')
        actions.each_pair do |type, keys|
          method = "write_#{sub}#{type}".gsub(/\[[\d]*\]/, '').to_sym
          if respond_to?(method, true)
            send(method, keys)
          else
            puts "WARNING: Cannot configure keys #{type}, NOT IMPLEMENTED"
          end
        end
      end

      def write_general(keys)
        @client.put("/projects/#{@project_id}", keys)
      end

      def write_protected_branches(key)
        id = @project_id
        @client.put("/projects/#{id}/repository/branches/#{key}/protect", {})
      end

      def write_unprotected_branches(key)
        id = @project_id
        @client.put("/projects/#{id}/repository/branches/#{key}/unprotect", {})
      end

      def write_git_hook(keys)
        @client.put("/projects/#{@project_id}/git_hook", keys)
      end

      def write_services(keys)
        execute(keys, 'service_')
      end

      def write_service_slack(keys)
        keys.merge!(keys.delete('properties')) unless keys['properties'].nil?
        keys.select! { |key| %w(username channel webhook).include?(key) }
        unless keys.empty?
          keys['webhook'] ||=
            @config['services']['slack']['properties']['webhook']
          @client.put("/projects/#{@project_id}/services/slack", keys)
        end
      end

      def write_runners(keys)
        @client.post("/projects/#{@project_id}/runners", runner_id: keys)
      end

      def write_unrunners(keys)
        @client.delete("/projects/#{@project_id}/runners/#{keys}", {})
      end
    end
  end
end
