#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'fileutils'

module Gitlab
  # Module Utils
  module Utils
    # Module Core
    module Core
      class << self
        def config(options)
          @config = Config.load(options)
        end

        def print_groups(client)
          groups = client.get('/groups')
          groups.each do |group|
            puts group['path'] + ' -> ' + 'Group ID:' + group['id'].to_s
          end
        end

        def print_projects(client, group, archived)
          archived = nil if archived
          group_projects = client.groups_projects(group, archived)
          group_projects.each_pair do |_name, project|
            puts "#{project['path_with_namespace']}:#{project['id']}"
          end
        end

        def clone(client, group, archived, localpath)
          archived = nil if archived
          group_projects = client.groups_projects(group, archived)
          group_projects.each_pair do |_name, project|
            group_path = File.join(localpath, project['namespace']['path'])
            FileUtils.mkpath(group_path)
            fetch_project(project, group_path)
          end
        end

        def fetch_project(project, group_path)
          git = Git.new(File.join(group_path, project['path']))
          if git.exist?
            git.pull('--ff-only')
          else
            git.clone(project['ssh_url_to_repo'])
          end
          puts git.last_out
        end
      end
    end
  end
end
