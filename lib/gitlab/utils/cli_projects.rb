#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'
require 'gitlab/utils/cli_options'

module Gitlab
  module Utils
    # projects subcommand
    class CliProjects < Thor
      desc 'list', 'List projects in all or a specific group'
      option(*CliOptions::ARCHIVED)
      option(*CliOptions::GROUP)
      def list
        config = Config.load(options)
        client = Client.new(config['endpoint'], config['token'])
        fetch_groups(client, config['group']).each do |group|
          Core.print_projects(client, group, config['archived'])
        end
      end

      desc 'show PROJECT_ID', 'Show project configuration'
      def show(project_id)
        config = Config.load(options)
        client = Client.new(config['endpoint'], config['token'])
        project = Project.new(client, project_id).update(config)
        pp project.config
      end

      desc 'check PROJECT_ID', 'Check project configuration'
      def check(project_id)
        config = Config.load(options)
        client = Client.new(config['endpoint'], config['token'])
        project = Project.new(client, project_id).update(config)
        project.print_diff
      end

      desc 'configure PROJECT_ID KEY VALUE', 'Set project configuration'
      def configure(project_id, key, value)
        config = Config.load(options)
        client = Client.new(config['endpoint'], config['token'])
        client.put("/projects/#{project_id}", key => value)
      end

      desc 'sync', 'Synchronize configuration for a group or a project'
      option(*CliOptions::GROUP)
      option(*CliOptions::PROJECT)
      option(*CliOptions::ARCHIVED)
      def sync
        config = Config.load(options)
        client = Client.new(config['endpoint'], config['token'])
        if config['project'] =~ /^[0-9]+$/
          sync_project(client, config, config['project'])
        else
          sync_projects(client, config)
        end
      end

      private

      def sync_project(client, config, project_id)
        project = Project.new(client, project_id).update(config)
        _actions = project.sync_config
        # project.update(config) unless actions.empty?
        # puts ''
        # project.print_diff
      end

      def sync_projects(client, config)
        whitelist = config['groups'].keys
        fetch_groups(client, config['group'], whitelist).each do |group|
          puts group
          projects = fetch_projects(client, group, config)
          projects.each do |project_id|
            sync_project(client, config, project_id)
            puts ''
          end
        end
      end

      def fetch_groups(client, group, list = [])
        if group.nil?
          groups = client.get('/groups').map { |g| g['path'] }
          groups.select { |g| list.include?(g) } unless list.empty?
        else
          [group]
        end
      end

      def fetch_projects(client, group, config)
        if config['project'].nil?
          projs = client.get(
            "/groups/#{group}/projects",
            archived: config['archived']
          )
          projs.map { |p| "#{group}%2F#{p['path']}" }
        else
          ["#{group}%2F#{config['project']}"]
        end
      end
    end
  end
end
