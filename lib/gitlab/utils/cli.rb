#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'thor'
require 'pp'
require 'gitlab/utils/cli_options'
require 'gitlab/utils/cli_projects'

module Gitlab
  module Utils
    # CLI
    class Cli < Thor
      # Exit 1 on failure
      def self.exit_on_failure?
        true
      end

      # global options
      class_option(
        :endpoint,
        desc: 'Gitlab endpoint',
        default: 'https://gitlab.com/api/v3',
        aliases: ['-e']
      )
      class_option(
        :token,
        desc: 'Private token associated with our Gitlab user',
        aliases: ['-t']
      )
      class_option(
        :config_file,
        desc: 'Configuration file to use',
        default: File.join(Dir.home, '.config', 'gitlab-utils.yml'),
        aliases: ['-c']
      )

      desc 'groups', 'List available groups'
      def groups
        config = Config.load(options)
        client = Client.new(config['endpoint'], config['token'])
        Core.print_groups(client)
      end

      desc 'clone', 'Clone/pull all projects of a group'
      option(*CliOptions::GROUP)
      option(*CliOptions::ARCHIVED)
      option(*CliOptions::LOCALPATH)
      def clone
        config = Config.load(options, ['localpath'])
        client = Client.new(config['endpoint'], config['token'])
        Core.clone(client,
                   config['group'], config['archived'], config['localpath'])
      end

      desc 'get ACTION', 'Execute a GET action'
      def get(action)
        config = Config.load(options)
        client = Client.new(config['endpoint'], config['token'])
        pp client.get(action)
      end

      desc 'projects SUBCOMMAND ...ARGS', 'Manage projects'
      subcommand 'projects', CliProjects
    end
  end
end
